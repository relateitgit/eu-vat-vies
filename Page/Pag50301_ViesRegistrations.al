page 50301 RitViesRegistrations
{
    PageType = List;
    UsageCategory = Lists;
    ApplicationArea = All;
    SourceTable = RitViesRegistration;
    CaptionML = DAN = 'VIES Registreringer', ENU = 'VIES Registrations';

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field(VatNumber; VatNumber)
                {
                    ApplicationArea = All;
                }
                field(CountryCode;CountryCode)              
                {
                    ApplicationArea = all;
                }

                field(ReqDate;ReqDate)
                {
                    ApplicationArea = All;
                }

                field(Valid;Valid)
                {
                    ApplicationArea = All;
                }
                //Swith to field from Approx Response
                field("VIES-CompanyType";"VIES-CompanyType")
                {
                    Visible = false;
                }
                field("VIES-Street";"VIES-Street")
                {
                    Visible = false;
                }
                field("VIES-PostCode";"VIES-PostCode")
                {
                    Visible =false;
                }
                field("VIES-City";"VIES-City")
                {
                    Visible = false;
                }
                field(ReqIdentifier;ReqIdentifier)
                {
                    Visible = false;
                }
                
                
            }
        }
        area(Factboxes)
        {
            
        }
    }
    
    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                
            }
        }
    }
}