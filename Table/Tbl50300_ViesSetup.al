table 50300 RitViesSetup
{
    DataClassification = ToBeClassified;
    CaptionML = DAN = 'Opsætning af VIES', ENU = 'VIES Setup';
    
    fields
    {
        field(1;"Primary Key"; Code[10])
        {
            CaptionML = DAN = 'Primær Nøgle', ENU = 'Primary Key';
            DataClassification = ToBeClassified;
        }

        field(2; "ExtendedRequest"; Boolean)
        {
            CaptionML = DAN = 'Udvidet VIES Forespørgsel', ENU = 'Extended VIES Request';
            DataClassification = ToBeClassified;
        }

        field(10;ViesServiceAddress;text[150])
        {
            CaptionML = DAN = 'VIES Adresse', ENU = 'VIES Address';
        }
        
        
    }
    
    keys
    {
        key(PK; "Primary Key")
        {
            Clustered = true;
        }
    }
    
}