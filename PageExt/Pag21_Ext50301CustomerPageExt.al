pageextension 50301 RitViesCustomerCardPageExt extends "Customer Card" 
{
    layout
    {
        addlast(General)
        {
            field(VatNoValid;VatNoValid)
            {
                Importance = Additional;
            }
        }
    }
    
    actions
    {
        addlast("&Customer")
        {
             group(Vies)
            {
                action(ViewViesRegistrations)
                {
                    ApplicationArea = All;
                    CaptionML = DAN = 'VIES Registreringer', ENU = 'VIES Registrations';
                    RunObject = page RitViesRegistrations;
                    RunPageLink = VatNumber = field("VAT Registration No.");
                    Image = ViewDetails;
                    
                    trigger OnAction()
                    begin
                                            
                    end;
                }

                action(CheckCustomerVatNo)
                {
                    ApplicationArea = All;
                    CaptionML = DAN = 'Check mod VIES', ENU = 'Check against VIES';
                    Image = Check;
                    
                    trigger OnAction()
                    var
                        ViesMgt: Codeunit RitViesMgt;
                    begin
                        ViesMgt.CheckVatNo(Rec);
                    end;
                }
            }
        }
    }
}