codeunit 50300 RitViesMgt
{
    var
        Client: HttpClient;
        ResponseMessage: HttpResponseMessage;
        ReqMessage: HttpRequestMessage;
        Content: HttpContent;
        Headers: HttpHeaders;

        ViesSetup: Record RitViesSetup;

        procedure CheckVatNo(VatCust:Record Customer)
        var
        begin
            ViesSetup.Get();
            if ViesSetup.ExtendedRequest then
                CheckVatNoApprox(VatCust)
            else
                CheckVatNoSimple(VatCust);
        end;
        procedure CheckVatNoSimple(VatCust:Record Customer)
            var
                oStream: OutStream;
                iStream: inStream;
                ViesSimpleReq: XmlPort RitViesSimpleRequest;
                tempBlob: Record TempBlob temporary;
                RespContent: HttpContent;
                ViesSimpleResponse: XmlPort RitViesSimpleResponse;
                txt: Text;

            begin
                VatCust.TestField("Country/Region Code");
                VatCust.TestField("VAT Registration No.");

                tempBlob.Blob.CreateOutStream(oStream);

                ViesSimpleReq.SetTableView(VatCust);
                ViesSimpleReq.SetDestination(oStream);
                ViesSimpleReq.Export();

                tempBlob.Blob.CreateInStream(iStream);

                Content.WriteFrom(iStream);
                
                Client.Post(ViesSetup.ViesServiceAddress,Content,ResponseMessage);

                if ResponseMessage.IsSuccessStatusCode() then begin
                    RespContent := ResponseMessage.Content();
                    RespContent.ReadAs(iStream);
                    RespContent.ReadAs(txt);

                    ViesSimpleResponse.SetSource(iStream);
                    ViesSimpleResponse.Import();
                end
                else begin
                end;
            end;
            procedure CheckVatNoApprox(VatCust: Record Customer)
            var
                oStream: OutStream;
                iStream: inStream;
                ViesApproxReq: XmlPort RitViesApproxRequest;
                tempBlob: Record TempBlob temporary;
                RespContent: HttpContent;
                ViesApproxResponse: XmlPort RitViesApproxResponse;

            begin
                VatCust.TestField("Country/Region Code");
                VatCust.TestField("VAT Registration No.");

                tempBlob.Blob.CreateOutStream(oStream);

                ViesApproxReq.SetTableView(VatCust);
                ViesApproxReq.SetDestination(oStream);
                ViesApproxReq.Export();

                tempBlob.Blob.CreateInStream(iStream);

                Content.WriteFrom(iStream);
                Client.Post(ViesSetup.ViesServiceAddress,Content,ResponseMessage);

                if ResponseMessage.IsSuccessStatusCode() then begin
                    message(format(ResponseMessage.HttpStatusCode()));
                    RespContent := ResponseMessage.Content();
                    RespContent.ReadAs(iStream);

                    ViesApproxResponse.SetSource(iStream);
                    ViesApproxResponse.Import();
                end
                else begin
                end;
            end;
        
}