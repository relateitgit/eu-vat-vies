table 50301 RitViesRegistration
{
    DataClassification = CustomerContent;
    
    fields
    {
        field(1;EntryNo; Integer)
        {
            captionML = DAN = 'Løbenr.', ENU = 'Entry No.';
            DataClassification = ToBeClassified;
            AutoIncrement = TRUE;
        }
        field(2; "VatNumber"; Code[20])
        {
            Captionml = DAN = 'MOMS Nr.', ENU = 'VAT No.';
            DataClassification = CustomerContent;
        }

        field(3; "CountryCode"; Code[10])
        {
            CaptionML = DAN = 'Landekode', ENU = 'Country Code';
            DataClassification = CustomerContent;
        }

        field(4; "ReqDate"; Date)
        {
            CaptionML = DAN = 'Forespørgselsdato', ENU = 'Request Date';
            DataClassification = ToBeClassified;
        }

        field(5; "Valid"; Boolean)
        {
            CaptionML = DAN = 'Gyldig', ENU = 'Valid';
            DataClassification = ToBeClassified;
        }
        
        field(10; "VIES-Name"; Text[100])
        {
            CaptionML = DAN = 'VIES-Navn', ENU = 'VIES-Name';
            DataClassification = CustomerContent;
        }
        
        field(11; "VIES-Address"; Text[100])
        {
            CaptionML = DAN = 'VIES-Adresse', ENU = 'VIES-Address';
            DataClassification = CustomerContent;
        }
        field(12;"VIES-Street";text[100])
        {
            CaptionML = DAN = 'VIES-Gade', ENU = 'VIES-Street';
            DataClassification = CustomerContent;
        }
        field(13; "VIES-PostCode"; Text[20])
        {
            CaptionML = DAN = 'VIES-Postnr.', ENU = 'VIES-Postcode';
            DataClassification = CustomerContent;
        }
        field(14; "VIES-City"; Text[50])
        {
            CaptionML = DAN = 'VIES-By', ENU = 'VIES-City';
            DataClassification = CustomerContent;
        }

        field(15; "VIES-CompanyType"; Text[10])
        {
            CaptionML = DAN = 'VIES-Virksomhedstype', ENU = 'VIES-CompanyType';
            DataClassification = ToBeClassified;
        }
        
        
        field(30; "NameMatch"; Text[10])
        {
            Caption = 'NameMatch';
            DataClassification = ToBeClassified;
        }

        field(31; "CompanyTypeMatch"; Text[10])
        {
            Caption = 'CompanyTypeMatch';
            DataClassification = ToBeClassified;
        }
        field(32; "StreetMatch"; Text[10])
        {
            Caption = 'StreetMatch';
            DataClassification = ToBeClassified;
        }
        field(33; "PostCodeMatch"; Text[10])
        {
            Caption = 'PostCodeMatch';
            DataClassification = ToBeClassified;
        }
        field(34; "CityMatch"; Text[10])
        {
            Caption = 'CityMatch';
            DataClassification = ToBeClassified;
        }
        
        
        
        
        
        field(50; "ReqIdentifier"; Text[30])
        {
            CaptionML = DAN = 'Identifikator', ENU = 'Request Identifier';
            DataClassification = ToBeClassified;
        }
        
        
    }
    
    keys
    {
        key(PK; EntryNo)
        {
            Clustered = true;
        }
        key(Key1;VatNumber,ReqDate,Valid)
        {
            
        }

        key(Key2; "VIES-Name","VIES-Address")
        {
            
        }
    }
    
}