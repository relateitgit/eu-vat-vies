xmlport 50302 RitViesApproxRequest
{
    Direction = Export;
    schema
    {
        textelement(Root)
        {
            tableelement(VatCustomer; Customer)
            {
                fieldelement(countryCode;VatCustomer."Country/Region Code")
                {}
                fieldelement(vatNumber;VatCustomer."VAT Registration No.")
                {}
                textelement(traderName)
                {
                    MinOccurs = Zero;
                }
                textelement(traderCompanyType)
                {
                    MinOccurs = Zero;
                }
                textelement(traderStreet)
                {
                    MinOccurs = Zero;
                }
                textelement(traderPostCode)
                {
                    MinOccurs = Zero;
                }
                textelement(traderCity)
                {
                    MinOccurs = Zero;
                }
                textelement(requesterCountryCode)
                {
                    MinOccurs = Zero;
                }
                textelement(requesterVatNumber)
                {
                    MinOccurs = Zero;
                }
                trigger
                OnAfterGetRecord()
                begin
                    CompInfo.Get();
                    traderName := CompInfo.Name;
                    traderStreet := CompInfo.Address;
                    traderPostCode := CompInfo."Post Code";
                    traderCity := CompInfo.City;
                    traderCompanyType := '';
                    requesterCountryCode := CompInfo."Country/Region Code";
                    requesterVatNumber := CompInfo."VAT Registration No.";
                end;
            }
        }
    }
    
    requestpage
    {
        layout
        {
            area(content)
            {
                group(GroupName)
                {
                    field(CustNo; VatCustomer."No.")
                    {
                        
                    }
                }
            }
        }
    }
    var
    CompInfo: Record "Company Information";
}