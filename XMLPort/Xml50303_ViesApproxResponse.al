xmlport 50303 RitViesApproxResponse
{
    Direction = Import;
    schema
    {
        textelement(Root)
        {
            tableelement(Loop; Integer)
            {
                AutoSave = false;
                textelement(countryCode)
                {}
                textelement(vatNumber)
                {}
                textelement(requestDate)
                {}
                textelement(valid)
                {}
                textelement(traderName)
                {
                    MinOccurs = Zero;
                }
                textelement(traderCompanyType)
                {
                    MinOccurs = Zero;
                }
                textelement(traderAddress)
                {
                    MinOccurs = Zero;
                }
                textelement(traderStreet)
                {
                    MinOccurs = Zero;
                }
                textelement(traderPostCode)
                {
                    MinOccurs = Zero;
                }
                textelement(traderCity)
                {
                    MinOccurs = Zero;
                }
                textelement(traderNameMatch)
                {
                    MinOccurs = Zero;
                }
                textelement(traderCompanyTypeMatch)
                {
                    MinOccurs = Zero;
                }
                textelement(traderStreetMatch)
                {
                    MinOccurs = Zero;
                }
                textelement(traderPostCodeMatch)
                {
                    MinOccurs = Zero;
                }
                textelement(traderCityMatch)
                {
                    MinOccurs = Zero;
                }
                textelement(requestIdentifier)
                {}
            }
            trigger OnAfterAssignVariable()
            var
                ViesRegistration: Record RitViesRegistration;
                Cust: Record Customer;
            begin
                clear(ViesRegistration);
                ViesRegistration.CountryCode := countryCode;
                evaluate(ViesRegistration.ReqDate,requestDate);
                ViesRegistration.VatNumber := vatNumber;
                Evaluate(ViesRegistration.Valid,valid);
                ViesRegistration.Insert(true);

                clear(cust);
                cust.SetFilter("VAT Registration No.",vatNumber);
                cust.ModifyAll(VatNoValid,ViesRegistration.Valid);
            end;
        }
    }
}