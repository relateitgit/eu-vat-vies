xmlport 50301 RitViesSimpleResponse
{
    Direction = Import;
    schema
    {
        textelement(Body)
        {
            tableelement(checkVatResponse; Integer)
            {
                AutoSave = false;
                textelement(countryCode)
                {}
                textelement(vatNumber)
                {}
                textelement(requestDate)
                {}
                textelement(valid)
                {}
                textelement(name)
                {
                    MinOccurs = Zero;
                }
                textelement(address)
                {
                    MinOccurs = Zero;
                }
            }
            trigger OnAfterAssignVariable()
            var
                ViesRegistration: Record RitViesRegistration;
                Cust: Record Customer;
            begin
                clear(ViesRegistration);
                ViesRegistration.CountryCode := countryCode;
                evaluate(ViesRegistration.ReqDate,requestDate);
                ViesRegistration.VatNumber := vatNumber;
                Evaluate(ViesRegistration.Valid,valid);
                ViesRegistration.Insert(true);

                clear(cust);
                cust.SetFilter("VAT Registration No.",vatNumber);
                cust.ModifyAll(VatNoValid,ViesRegistration.Valid);
            end;
        }
    }
}