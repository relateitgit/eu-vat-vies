xmlport 50300 RitViesSimpleRequest
{
    Direction = Export;
    
    schema
    {
        textelement(Root)
        {
            tableelement(VatCustomer; Customer)
            {
                fieldelement(CountryCode;VatCustomer."Country/Region Code")
                {}
                fieldelement(VatNumber;VatCustomer."VAT Registration No.")
                {}
            }    
        }
    }
    
    requestpage
    {
        layout
        {
            area(content)
            {
                group(GroupName)
                {
                    field(CustNo; VatCustomer."No.")
                    {
                        
                    }
                }
            }
        }
    }
}