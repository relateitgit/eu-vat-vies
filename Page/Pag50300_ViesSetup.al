page 50300 RitViesSetup
{
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = RitViesSetup;
    InsertAllowed = false;
    
    layout
    {
        area(Content)
        {
            group(Basic)
            {
                CaptionML = DAN = 'Basis', ENU = 'Basic';
                field(ExtendedRequest;ExtendedRequest)
                {
                    ApplicationArea = All;
                }

                field(ViesServiceAddress;ViesServiceAddress)
                {
                    ApplicationArea = All;
                }
                
            }
        }
    }
    
    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;
                
                trigger OnAction()
                begin
                    
                end;
            }
        }
    }
    trigger
    OnOpenPage()
    begin
        if not get then begin
            init;
            insert;
        end;
    end;
}