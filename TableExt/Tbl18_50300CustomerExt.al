tableextension 50300 RitViesCustomerExt extends Customer 
{
    fields
    {
        field(50300; "VatNoValid"; Boolean)
        {
            CaptionML = DAN = 'Moms Nr. Gyldig', ENU = 'VAT No. Valid';
            DataClassification = ToBeClassified;
        }
        
    }
    
}